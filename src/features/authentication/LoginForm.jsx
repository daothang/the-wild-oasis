import {useEffect, useState} from "react";
import Button from "../../ui/Button";
import Form from "../../ui/Form";
import Input from "../../ui/Input";
import FormRowVertical from "../../ui/FormRowVertical";
import {useLogin} from "./useLogin.js";
import SpinnerMini from "../../ui/SpinnerMini.jsx";
import {useNavigate} from "react-router-dom";
import {useUser} from "./useUser.js";

function LoginForm() {
    const [email, setEmail] = useState("daothang.net@gmail.com");
    const [password, setPassword] = useState("12345678");
    const {login, isLoading, role} = useLogin();

    const navigate = useNavigate();
    //1. Load the authenticated user
    const {isLoading:isAuthenticating, isAuthenticated} = useUser();


    //2. If there is NO authenticated user, redirect to the login
    useEffect(function () {
        if (isAuthenticated && !isAuthenticating) {
            navigate("/dashboard");
        }
    }, [isAuthenticating, isAuthenticated, navigate])


    function handleSubmit(e) {
        e.preventDefault();
        if (!email || !password) return;
        login({email, password});
    }

    return (
        <Form onSubmit={handleSubmit}>
            <FormRowVertical label="Email address">
                <Input
                    disabled={isLoading}
                    type="email"
                    id="email"
                    // This makes this form better for password managers
                    autoComplete="username"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                />
            </FormRowVertical>
            <FormRowVertical label="Password">
                <Input
                    disabled={isLoading}
                    type="password"
                    id="password"
                    autoComplete="current-password"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                />
            </FormRowVertical>
            <FormRowVertical>
                <Button disabled={isLoading} size="large">{!isLoading ? 'Login' : <SpinnerMini/>}</Button>
            </FormRowVertical>
        </Form>
    );
}

export default LoginForm;
