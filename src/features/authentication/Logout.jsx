import React from 'react';
import ButtonIcon from "../../ui/ButtonIcon.jsx";
import {HiLogout} from "react-icons/hi";
import {useLogout} from "./useLogout.js";
import {HiArrowRightOnRectangle} from "react-icons/hi2";
import SpinnerMini from "../../ui/SpinnerMini.jsx";

function Logout() {
    const {logout, isLoading} = useLogout();
    return (
        <ButtonIcon
            onClick={logout}
            disabled={isLoading}
        >
            {!isLoading ? <HiArrowRightOnRectangle/> : <SpinnerMini/>}</ButtonIcon>);
}

export default Logout;