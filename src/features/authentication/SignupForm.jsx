import Button from "../../ui/Button";
import Form from "../../ui/Form";
import FormRow from "../../ui/FormRow";
import Input from "../../ui/Input";
import {useForm} from "react-hook-form";
import {useSignup} from "./useSignup.js";

// Email regex: /\S+@\S+\.\S+/

function SignupForm() {
    const {register, formState, getValues, handleSubmit, reset} = useForm();
    const {errors} = formState;
    const {signup, isLoading} = useSignup();

    function onSubmit({email, password, fullName}) {
        signup({email, password, fullName});
    }

    return (
        <Form onSubmit={handleSubmit(onSubmit)}>
            <FormRow label="Full name" error={errors?.fullName?.message}>
                <Input type="text" id="fullName" disabled={isLoading} {...register("fullName", {required: "this field is required"})}/>
            </FormRow>

            <FormRow label="Email address" error={errors?.email?.message}>
                <Input type="email" id="email" disabled={isLoading} {...register("email", {
                    required: "this field is required",
                    pattern: {value: /\S+@\S+\.\S+/, message: "Please provide a valid email address"}
                })}/>
            </FormRow>

            <FormRow label="Password (min 8 characters)" error={errors?.password?.message}>
                <Input type="password" id="password" disabled={isLoading} {...register("password", {
                    required: "this field is required",
                    minLength: {value: 8, message: "Password need a minium of 8 characters"}
                })}/>
            </FormRow>

            <FormRow label="Repeat password" error={errors?.passwordConfirm?.message}>
                <Input type="password" id="passwordConfirm" disabled={isLoading} {...register("passwordConfirm", {
                    required: "this field is required",
                    validate: (value) => value === getValues().password || "Password needs to match"

                })}/>
            </FormRow>

            <FormRow>
                {/* type is an HTML attribute! */}
                <Button onClick={reset} variation="secondary" disabled={isLoading} type="reset">
                    Cancel
                </Button>
                <Button disabled={isLoading}>Create new user</Button>
            </FormRow>
        </Form>
    );
}

export default SignupForm;
