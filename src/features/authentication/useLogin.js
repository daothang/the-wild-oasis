import {useMutation, useQueryClient} from "@tanstack/react-query";
import {loginApi} from "../../services/apiAuth.js";
import toast from "react-hot-toast";
import {useNavigate} from "react-router-dom";

export function useLogin() {
    const navigate = useNavigate();
    const queryClient = useQueryClient();
    const {mutate: login, isLoading} = useMutation({
        mutationFn: ({email, password}) => loginApi({email, password}),
        onSuccess: data => {
            queryClient.setQueriesData(["user"], data.user);
            navigate("/dashboard", {replace: true});
        },
        onError: error => {
            console.log("ERROR: ", error);
            toast.error("Provider email and password are incorrect.")
        }
    })
    return {login, isLoading};
}