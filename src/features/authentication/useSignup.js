import {useMutation} from "@tanstack/react-query";
import {signup as signupApi} from "../../services/apiAuth.js";
import toast from "react-hot-toast";

export function useSignup() {
   const {mutate: signup, isLoading} = useMutation({
        mutationFn: signupApi,
        onSuccess: (user) => {
            toast.success("Signup a new user successfully, please check email to confirm registration")
        },
        onError: (error) => {
            console.log(error);
            toast.error("There was an error while registration processing.")
        }
    })
    return {signup, isLoading};
}