import {useMutation, useQueryClient} from "@tanstack/react-query";
import {updateUser as updateUserApi} from "../../services/apiAuth.js";
import toast from "react-hot-toast";

export function useUpdateUser() {
    const queryClient = useQueryClient();
    const {mutate: updateUser, isLoading: isUpdating} = useMutation({
        mutationFn: updateUserApi,
        onSuccess: ({user}) => {
            queryClient.setQueriesData(['user'], user);
            toast.success("Update the user successfully")
        },
        onError: (error) => {
            console.log(error);
            toast.error("There was an error while updating processing.")
        }
    })
    return {updateUser, isUpdating};
}