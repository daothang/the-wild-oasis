import styled from "styled-components";

import BookingDataBox from "./BookingDataBox";
import Row from "../../ui/Row";
import Heading from "../../ui/Heading";
import Tag from "../../ui/Tag";
import ButtonGroup from "../../ui/ButtonGroup";
import Button from "../../ui/Button";
import ButtonText from "../../ui/ButtonText";

import {useMoveBack} from "../../hooks/useMoveBack";
import {useBooking} from "./useBooking.js";
import Spinner from "../../ui/Spinner.jsx";
import {useNavigate} from "react-router-dom";
import Modal from "../../ui/Modal.jsx";
import ConfirmDelete from "../../ui/ConfirmDelete.jsx";
import {useDeleteBooking} from "./useDeleteBooking.js";
import Empty from "../../ui/Empty.jsx";

const HeadingGroup = styled.div`
    display: flex;
    gap: 2.4rem;
    align-items: center;
`;

function BookingDetail() {
    const {booking, isLoading, error} = useBooking();
    const navigate = useNavigate();


    const moveBack = useMoveBack();
    const {deleteBookingMutate, isDeleting} = useDeleteBooking();

    const statusToTagName = {
        unconfirmed: "blue", "checked-in": "green", "checked-out": "silver",
    };

    if (isLoading) return <Spinner/>
    if(!booking || !Object.keys(booking).length) return <Empty resource="Booking" />

    const {status, id: bookingId} = booking
    return (<>
            <Row type="horizontal">
                <HeadingGroup>
                    <Heading as="h1">Booking #{bookingId}</Heading>
                    <Tag type={statusToTagName[status]}>{status.replace("-", " ")}</Tag>
                </HeadingGroup>
                <ButtonText onClick={moveBack}>&larr; Back</ButtonText>
            </Row>

            <BookingDataBox booking={booking}/>

            <ButtonGroup>
                {status !== "checked-in" &&
                    <Button variation="primary" onClick={() => navigate(`/checkin/${bookingId}`)}>
                        Checkin
                    </Button>}
                <Modal>
                    <Modal.Open opensWindowName="booking-deletion">
                        <Button variation="danger">
                            Delete
                        </Button>
                    </Modal.Open>
                    <Modal.ModalWindow name="booking-deletion">
                        <ConfirmDelete disabled={isDeleting} resourceName="Booking"
                                       onConfirm={() => deleteBookingMutate(bookingId, {onSettled: () => navigate(-1)})}/>
                    </Modal.ModalWindow>
                </Modal>
                <Button variation="secondary" onClick={moveBack}>
                    Back
                </Button>
            </ButtonGroup>
        </>);
}

export default BookingDetail;
