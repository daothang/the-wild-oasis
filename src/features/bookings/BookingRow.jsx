import styled from "styled-components";
import {format, isToday} from "date-fns";

import Tag from "../../ui/Tag";
import Table from "../../ui/Table";

import {formatCurrency, formatDistanceFromNow} from "../../utils/helpers";
import Menus from "../../ui/Menus.jsx";
import {IoEyeOutline} from "react-icons/io5";
import {useNavigate} from "react-router-dom";
import {FaRegCheckSquare} from "react-icons/fa";
import {GiExitDoor} from "react-icons/gi";
import {useCheckout} from "../check-in-out/useCheckout.js";
import {MdDeleteOutline} from "react-icons/md";
import {useDeleteBooking} from "./useDeleteBooking.js";
import Modal from "../../ui/Modal.jsx";
import ConfirmDelete from "../../ui/ConfirmDelete.jsx";

const Cabin = styled.div`
    font-size: 1.6rem;
    font-weight: 600;
    color: var(--color-grey-600);
    font-family: "Sono";
`;

const Stacked = styled.div`
    display: flex;
    flex-direction: column;
    gap: 0.2rem;

    & span:first-child {
        font-weight: 500;
    }

    & span:last-child {
        color: var(--color-grey-500);
        font-size: 1.2rem;
    }
`;

const Amount = styled.div`
    font-family: "Sono";
    font-weight: 500;
`;

function BookingRow({
                        booking: {
                            id: bookingId,
                            created_at,
                            startDate,
                            endDate,
                            numNights,
                            numGuests,
                            totalPrice,
                            status,
                            guests: {fullName: guestName, email},
                            cabins: {name: cabinName},
                        },
                    }) {
    const navigate = useNavigate()
    const {checkout, isCheckingOut} = useCheckout();
    const {deleteBookingMutate, isDeleting} = useDeleteBooking();
    const statusToTagName = {
        unconfirmed: "blue",
        "checked-in": "green",
        "checked-out": "silver",
    };

    return (

        <Table.Row>
            <Cabin>{cabinName}</Cabin>

            <Stacked>
                <span>{guestName}</span>
                <span>{email}</span>
            </Stacked>

            <Stacked>
        <span>
          {isToday(new Date(startDate))
              ? "Today"
              : formatDistanceFromNow(startDate)}{" "}
            &rarr; {numNights} night stay
        </span>
                <span>
          {format(new Date(startDate), "MMM dd yyyy")} &mdash;{" "}
                    {format(new Date(endDate), "MMM dd yyyy")}
        </span>
            </Stacked>

            <Tag type={statusToTagName[status]}>{status.replace("-", " ")}</Tag>

            <Amount>{formatCurrency(totalPrice)}</Amount>

            <Modal>
                <Menus.Menu>
                    <Menus.Toggle id={bookingId}/>
                    <Menus.List id={bookingId}>
                        <Menus.Button onClick={() => navigate(`${bookingId}`)} icon={<IoEyeOutline/>
                        }>
                            see details
                        </Menus.Button>
                        {status === 'unconfirmed' &&
                            <Menus.Button onClick={() => navigate(`/checkin/${bookingId}`)} icon={<FaRegCheckSquare/>}>Checked
                                in</Menus.Button>}
                        {status === 'checked-in' &&
                            <Menus.Button icon={<GiExitDoor/>} onClick={() => checkout(bookingId)}>
                                Check out
                            </Menus.Button>}
                        {status !== 'checked-in' && <Modal.Open opensWindowName="booking-deletion">
                            <Menus.Button icon={<MdDeleteOutline/>}>
                                Delete
                            </Menus.Button>
                        </Modal.Open>}

                    </Menus.List>
                </Menus.Menu>
                <Modal.ModalWindow name="booking-deletion">
                    <ConfirmDelete disabled={isDeleting} resourceName="Booking"
                                   onConfirm={() => deleteBookingMutate(bookingId)}/>
                </Modal.ModalWindow>
            </Modal>
        </Table.Row>
    );
}

export default BookingRow;
