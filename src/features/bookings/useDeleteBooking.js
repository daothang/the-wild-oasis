import {useMutation, useQuery, useQueryClient} from "@tanstack/react-query";
import {deleteBooking} from "../../services/apiBookings.js";
import toast from "react-hot-toast";

export function useDeleteBooking() {
    const queryClient = useQueryClient();
   const {isLoading: isDeleting, mutate: deleteBookingMutate} = useMutation({
        mutationFn: (bookingId) => deleteBooking(bookingId),
        onSuccess: () => {
            toast.success("Delete the booking successfully");
            queryClient.invalidateQueries(["bookings"]);
        },
        onError: error => toast.error("there was an error while booking deletion")
    })

    return {isDeleting, deleteBookingMutate};
}