import React from 'react';
import Button from "../../ui/Button.jsx";
import CreateCabinForm from "./CreateCabinForm.jsx";
import Modal from "../../ui/Modal.jsx";

function AddCabin() {

    return (
        <Modal>
            <Modal.Open opensWindowName='cabin-form'>
                <Button>Add new cabin</Button>
            </Modal.Open>
            <Modal.ModalWindow name='cabin-form'>
                <CreateCabinForm/>
            </Modal.ModalWindow>
        </Modal>

        // <Modal>
        //     <Modal.ModalOpen opens='table'>
        //         <Button>Add new cabin</Button>
        //     </Modal.ModalOpen>
        //     <Modal.ModalWindow name='table'>
        //         <CreateCabinForm/>
        //     </Modal.ModalWindow>
        // </Modal>
    )

    // const [showForm, setShowForm] = useState(false);
    //
    // return (
    //     <>
    //         <Button onClick={() => setShowForm(showForm => !showForm)}>Add new cabin</Button>
    //         {showForm && <Modal onClose={() => setShowForm(false)}>
    //             <CreateCabinForm onCloseModal={() => setShowForm(false)}/>
    //         </Modal>}
    //     </>
    // );
}

export default AddCabin;