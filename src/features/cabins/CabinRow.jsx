import styled from "styled-components";
import {formatCurrency} from "../../utils/helpers.js";
import {useState} from "react";
import CreateCabinForm from "./CreateCabinForm.jsx";
import useDeleteCabin from "./useDeleteCabin.js";
import {GoDuplicate} from "react-icons/go";
import {HiOutlinePencil} from "react-icons/hi2";
import {MdDeleteOutline} from "react-icons/md";
import {useCreateCabin} from "./useCreateCabin.js";
import Modal from "../../ui/Modal.jsx";
import ConfirmDelete from "../../ui/ConfirmDelete.jsx";
import Table from "../../ui/Table.jsx";
import Menus from "../../ui/Menus.jsx";

const Img = styled.img`
    display: block;
    width: 6.4rem;
    aspect-ratio: 3 / 2;
    object-fit: cover;
    object-position: center;
    transform: scale(1.5) translateX(-7px);
`;

const Cabin = styled.div`
    font-size: 1.6rem;
    font-weight: 600;
    color: var(--color-grey-600);
    font-family: "Sono";
`;

const Price = styled.div`
    font-family: "Sono";
    font-weight: 600;
`;

const Discount = styled.div`
    font-family: "Sono";
    font-weight: 500;
    color: var(--color-green-700);
`;


function CabinRow({cabin}) {
    const {id: cabinId, name, image, maxCapacity, regularPrice, discount, description} = cabin;

    const {isDeleting, deleteCabin} = useDeleteCabin()
    const {isCreatingCabin, createCabin} = useCreateCabin();

    function duplicateCabin() {
        createCabin({name: `copy of ${name}`, image, maxCapacity, regularPrice, discount, description})
    }

    return (
        <Table.Row role='row'>
            <Img src={image}/>
            <Cabin>{name}</Cabin>
            <div>Fits up tp {maxCapacity} guests</div>
            <Price>{formatCurrency(regularPrice)}</Price>
            {discount ? <Discount>{formatCurrency(discount)}</Discount> : <span>&mdash;</span>}
            <div>
                <Modal>
                    <Menus.Menu>
                        <Menus.Toggle id={cabinId}/>
                        <Menus.List id={cabinId}>

                            <Menus.Button icon={<GoDuplicate/>} onClick={duplicateCabin}>Duplicate</Menus.Button>

                            <Modal.Open opensWindowName='cabin-edit'>
                                <Menus.Button icon={<HiOutlinePencil/>}>
                                    Edit
                                </Menus.Button>
                            </Modal.Open>
                            <Menus.Button icon={<MdDeleteOutline/>} onClick={() => deleteCabin(cabinId)}>Delete</Menus.Button>
                        </Menus.List>

                        <Modal.ModalWindow name='cabin-edit'><CreateCabinForm cabinToEdit={cabin}/></Modal.ModalWindow>

                        <Modal.ModalWindow name='delete-confirm'>
                            <ConfirmDelete disabled={isDeleting} resourceName='Cabin'
                                           onConfirm={() => deleteCabin(cabinId)}/>
                        </Modal.ModalWindow>
                    </Menus.Menu>
                </Modal>

            </div>
        </Table.Row>
    );
}

export default CabinRow;