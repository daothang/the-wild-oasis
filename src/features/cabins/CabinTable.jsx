import Spinner from "../../ui/Spinner.jsx";
import CabinRow from "./CabinRow.jsx";
import {useLoadCabin} from "./useLoadCabin.js";
import Table from "../../ui/Table.jsx";
import Menus from "../../ui/Menus.jsx";
import {useSearchParams} from "react-router-dom";
import Empty from "../../ui/Empty.jsx";
import Pagination from "../../ui/Pagination.jsx";

function CabinTable() {
    const [searchParams] = useSearchParams();
    const {isLoading, cabins, count} = useLoadCabin()
    if (isLoading) return <Spinner/>
    if(!cabins) return <Empty resource="Bookings" />

    const filterValue = searchParams.get('discount') || 'all';
    let filterCabins;
    if(filterValue === 'all') filterCabins = cabins;
    if(filterValue === 'discount') filterCabins = cabins.filter(cabin => cabin.discount > 0);
    if(filterValue === 'no-discount') filterCabins = cabins.filter(cabin => cabin.discount === 0);

    const sortedValue = searchParams.get('sortBy') || 'name-asc';
    const [field, direction] = sortedValue.split("-");
    const modifier = direction === 'asc' ? 1 : -1;
    const sortedCabins = filterCabins.sort((a,b)=> (a[field]-b[field]) * modifier);

    return (
        <Menus>
            <Table role="table" columns='0.6fr 1.8fr 2.2fr 1fr 1fr 1fr'>
                <Table.Header role='row'>
                    <div></div>
                    <div>Cabin</div>
                    <div>Capacity</div>
                    <div>Price</div>
                    <div>Discount</div>
                </Table.Header>
                <Table.Body data={sortedCabins} render={(cabin) => (
                    <CabinRow cabin={cabin} key={cabin.id}/>
                )}>
                </Table.Body>
                <Table.Footer>
                    <Pagination count={count} />
                </Table.Footer>
            </Table>
        </Menus>
    );
}

export default CabinTable;