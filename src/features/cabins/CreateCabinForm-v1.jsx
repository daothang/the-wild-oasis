import {useForm} from "react-hook-form";
import {useMutation, useQueryClient} from "@tanstack/react-query";

import styled from "styled-components";
import Input from "../../ui/Input";
import Form from "../../ui/Form";
import Button from "../../ui/Button";
import FileInput from "../../ui/FileInput";
import Textarea from "../../ui/Textarea";

import {createEditCabin} from "../../services/apiCabins.js";
import toast from "react-hot-toast";
import FormRow from "../../ui/FormRow.jsx";


const StyledFormRow = styled.div`
    display: grid;
    align-items: center;
    grid-template-columns: 24rem 1fr 1.2fr;
    gap: 2.4rem;

    padding: 1.2rem 0;

    &:first-child {
        padding-top: 0;
    }

    &:last-child {
        padding-bottom: 0;
    }

    &:not(:last-child) {
        border-bottom: 1px solid var(--color-grey-100);
    }

    &:has(button) {
        display: flex;
        justify-content: flex-end;
        gap: 1.2rem;
    }
`;
function CreateCabinForm() {

    const queryClient = useQueryClient();
    const {formState, getValues, reset, register, handleSubmit} = useForm();
    const {errors} = formState;
    const {isLoading: isAdding, mutate} = useMutation({
        mutationFn: createEditCabin,
        // mutationFn: newCabin => createEditCabin(newCabin),
        onSuccess: data => {
            toast.success("cabin successfully created");
            queryClient.invalidateQueries({queryKey: ['cabins']});
            reset();
        },
        onError: error => {
            toast.error(error.message);

        }
    })

    function onSubmit(data) {
        mutate({...data, image: data.image[0]});
    }

    function onError(errors) {
        // console.log(errors)
    }

    return (
        <Form onSubmit={handleSubmit(onSubmit, onError)}>
            <FormRow label="Cabin name" error={errors?.name?.message}>
                <Input type="text" id="name" disabled={isAdding} {...register("name", {required: "this field is required"})}/>
            </FormRow>

            <FormRow label="Maximum capacity" error={errors?.maxCapacity?.message}>
                <Input type="number" id="maxCapacity" disabled={isAdding} {...register("maxCapacity", {
                    required: "this field is required",
                    min: {
                        value: 1,
                        message: "Capacity should be at least 1"
                    }
                })}/>
            </FormRow>

            <FormRow label="Regular price" error={errors?.regularPrice?.message}>
                <Input type="number"
                       id="regularPrice"
                       disabled={isAdding}
                       {...register("regularPrice", {required: "this field is required"})}/>
            </FormRow>

            <FormRow label="Discount" error={errors?.discount?.message}>
                <Input type="number" id="discount" disabled={isAdding} defaultValue={0} {...register("discount", {
                    required: "this field is required",
                    validate: (value) => value <= getValues().regularPrice || 'Discount should be less than regular price'
                })}/>

            </FormRow>

            <FormRow label="Description for website" error={errors?.description?.message}>
                <Textarea
                    type="number"
                    id="description"
                    disabled={isAdding}
                    {...register("description", {required: "this field is required"})}/>
            </FormRow>

            <FormRow label="Cabin photo">
                <FileInput id="image" accept="image/*" {...register('image')}/>
            </FormRow>

            <StyledFormRow>
                {/* type is an HTML attribute! */}
                <Button variation="secondary" type="reset">
                    Cancel
                </Button>
                <Button disabled={isAdding}>Add cabin</Button>
            </StyledFormRow>
        </Form>
    );
}

export default CreateCabinForm;
