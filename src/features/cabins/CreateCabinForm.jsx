import {useForm} from "react-hook-form";

import styled from "styled-components";
import Input from "../../ui/Input";
import Form from "../../ui/Form";
import Button from "../../ui/Button";
import FileInput from "../../ui/FileInput";
import Textarea from "../../ui/Textarea";

import FormRow from "../../ui/FormRow.jsx";
import {useCreateCabin} from "./useCreateCabin.js";
import {useEditCabin} from "./useEditCabin.js";


const StyledFormRow = styled.div`
    display: grid;
    align-items: center;
    grid-template-columns: 24rem 1fr 1.2fr;
    gap: 2.4rem;

    padding: 1.2rem 0;

    &:first-child {
        padding-top: 0;
    }

    &:last-child {
        padding-bottom: 0;
    }

    &:not(:last-child) {
        border-bottom: 1px solid var(--color-grey-100);
    }

    &:has(button) {
        display: flex;
        justify-content: flex-end;
        gap: 1.2rem;
    }
`;
function CreateCabinForm({cabinToEdit = {}, onCloseModal}) {

    //Create
    const {isCreatingCabin,createCabin} = useCreateCabin()
    //Edit
    const {isEditing, editMutable} = useEditCabin();
    const isWorking = isEditing || isCreatingCabin;

    const {id: editId, ...editValues} = cabinToEdit;
    const isEditSession = Boolean(editId);

    const {formState, getValues, reset, register, handleSubmit} = useForm({
        defaultValues: isEditSession ? editValues : {}
    });
    const {errors} = formState;

    function onSubmit(data) {
        const image = typeof data.image === 'string' ? data.image : data.image[0];
        if(isEditSession){
            editMutable({newCabinData : {...data, image: image}, id: editId}, {
                onSuccess: (data)=> {
                    reset();
                    onCloseModal?.()
                }
            });
        }else {
            createCabin({...data, image: image},{
                onSuccess: (data)=> {
                    reset();
                    onCloseModal?.()
                }
            });
        }
    }

    function onError(errors) {
        // console.log(errors)
    }

    return (
        <Form onSubmit={handleSubmit(onSubmit, onError)} type={onCloseModal ? 'modal' : 'regular'}>
            <FormRow label="Cabin name" error={errors?.name?.message}>
                <Input type="text" id="name" disabled={isWorking} {...register("name", {required: "this field is required"})}/>
            </FormRow>

            <FormRow label="Maximum capacity" error={errors?.maxCapacity?.message}>
                <Input type="number" id="maxCapacity" disabled={isWorking} {...register("maxCapacity", {
                    required: "this field is required",
                    min: {
                        value: 1,
                        message: "Capacity should be at least 1"
                    }
                })}/>
            </FormRow>

            <FormRow label="Regular price" error={errors?.regularPrice?.message}>
                <Input type="number"
                       id="regularPrice"
                       disabled={isWorking}
                       {...register("regularPrice", {required: "this field is required"})}/>
            </FormRow>

            <FormRow label="Discount" error={errors?.discount?.message}>
                <Input type="number" id="discount" disabled={isWorking} defaultValue={0} {...register("discount", {
                    required: "this field is required",
                    validate: (value) => value <= getValues().regularPrice || 'Discount should be less than regular price'
                })}/>

            </FormRow>

            <FormRow label="Description for website" error={errors?.description?.message}>
                <Textarea
                    type="number"
                    id="description"
                    disabled={isWorking}
                    {...register("description", {required: "this field is required"})}/>
            </FormRow>

            <FormRow label="Cabin photo">
                <FileInput id="image" accept="image/*" {...register('image', {required: isEditSession ? false : "this field is required"})} />
            </FormRow>

            <StyledFormRow>
                {/* type is an HTML attribute! */}
                <Button onClick={()=>onCloseModal?.()} variation="secondary" type="reset">
                    Cancel
                </Button>
                <Button disabled={isWorking}>{isEditSession ? ' Edit cabin' : 'Add cabin'}</Button>
            </StyledFormRow>
        </Form>
    );
}

export default CreateCabinForm;
