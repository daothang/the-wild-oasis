import React from 'react';
import Select from "../../ui/Select.jsx";
import {useSearchParams} from "react-router-dom";

function SortBy({options, type}) {
    const [searchParams, setSearchParams] = useSearchParams();

    const sortBy = searchParams.get('sortBy') || options.at(0).value
    function handleChange(e){
        searchParams.set('sortBy', e.target.value);
        setSearchParams(searchParams);
    }
    return (
        <div><Select value={sortBy} options={options} type={type} onChange={handleChange} /> </div>
    );
}

export default SortBy;