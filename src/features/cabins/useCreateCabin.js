import {useMutation, useQueryClient} from "@tanstack/react-query";
import {createEditCabin} from "../../services/apiCabins.js";
import toast from "react-hot-toast";

export function useCreateCabin(){
    const queryClient = useQueryClient();
    const {isLoading: isCreatingCabin, mutate: createCabin} = useMutation({
        mutationFn: createEditCabin,
        // mutationFn: newCabin => createEditCabin(newCabin),
        onSuccess: data => {
            toast.success("cabin successfully created");
            queryClient.invalidateQueries({queryKey: ['cabins']});
        },
        onError: error => {
            toast.error(error.message);

        }
    });
    return {isCreatingCabin, createCabin}
}