import {useMutation, useQueryClient} from "@tanstack/react-query";
import {createEditCabin} from "../../services/apiCabins.js";
import toast from "react-hot-toast";

export function useEditCabin(){
    const queryClient = useQueryClient();

    const {isLoading: isEditing, mutate:editMutable} = useMutation({
        // mutationFn: createEditCabin,
        mutationFn: ({newCabinData, id}) => createEditCabin(newCabinData, id),
        onSuccess: data => {
            toast.success("cabin successfully edited");
            queryClient.invalidateQueries({queryKey: ['cabins']});
        },
        onError: error => {
            toast.error(error.message);

        }
    })
    return {isEditing, editMutable};
}