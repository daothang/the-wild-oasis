import {useQuery, useQueryClient} from "@tanstack/react-query";
import {getCabins} from "../../services/apiCabins.js";
import {useSearchParams} from "react-router-dom";
import {PAGE_SIZE} from "../../utils/constants.js";

export function useLoadCabin(){
    const [searchParams] = useSearchParams();
    const queryClient = useQueryClient();

    const page = !searchParams.get("page") ? 1 : Number(searchParams.get("page"));
    const {isLoading, data: {cabins, count} = {}} = useQuery({
        queryKey: ['cabins', page],
        queryFn: () =>getCabins({page})
    });

    const numberPage = Math.ceil(count/PAGE_SIZE);
    //PRE-FETCHING
    if(page < numberPage){
        queryClient.prefetchQuery({
            queryKey: ['cabins', page + 1],
            queryFn: () =>getCabins({page: page + 1})
        });
    }
    if(page > 1){
        queryClient.prefetchQuery({
            queryKey: ['cabins', page - 1],
            queryFn: () =>getCabins({page: page - 1})
        });
    }
    return {isLoading, cabins, count};
}