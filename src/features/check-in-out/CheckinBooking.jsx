import styled from "styled-components";
import BookingDataBox from "../../features/bookings/BookingDataBox";

import Row from "../../ui/Row";
import Heading from "../../ui/Heading";
import ButtonGroup from "../../ui/ButtonGroup";
import Button from "../../ui/Button";
import ButtonText from "../../ui/ButtonText";

import {useMoveBack} from "../../hooks/useMoveBack";
import {useBooking} from "../bookings/useBooking.js";
import Spinner from "../../ui/Spinner.jsx";
import Checkbox from "../../ui/Checkbox.jsx";
import {useEffect, useState} from "react";
import {formatCurrency} from "../../utils/helpers.js";
import {useCheckin} from "./useCheckin.js";
import {useSettings} from "../settings/useSettings.js";

const Box = styled.div`
    /* Box */
    background-color: var(--color-grey-0);
    border: 1px solid var(--color-grey-100);
    border-radius: var(--border-radius-md);
    padding: 2.4rem 4rem;
`;

function CheckinBooking() {
    const moveBack = useMoveBack();
    const {checkin, isChecking} = useCheckin();
    const [confirmed, setConfirmed] = useState(false);
    const [addBreakfast, setBreakfast] = useState(false)
    const {booking, isLoading} = useBooking();
    const {settings: {breakfastPrice} = {}, isLoading: isSettingLoading} = useSettings();

    useEffect(() => {
        setConfirmed(booking?.isPaid || false)
    }, [booking]);

    const {
        id: bookingId,
        guests,
        totalPrice,
        numGuests,
        hasBreakfast,
        numNights,
    } = booking;

    const optionalBreakfastPrice = breakfastPrice * numNights * numGuests;

    function handleCheckin() {
        if (!confirmed) return;

        if (addBreakfast) {
            checkin({bookingId, breakfast: {
                totalPrice: optionalBreakfastPrice + totalPrice,
                    extrasPrice: optionalBreakfastPrice,
                    status: "checked-in",
                    isPaid: true
                }});
        } else {
            checkin({bookingId, breakfast: {}});
        }
    }

    if (isLoading || isSettingLoading) return <Spinner/>
    return (
        <>
            <Row type="horizontal">
                <Heading as="h1">Check in booking #{bookingId}</Heading>
                <ButtonText onClick={moveBack}>&larr; Back</ButtonText>
            </Row>

            <BookingDataBox booking={booking}/>

            {!hasBreakfast && <Box>
                <Checkbox
                    checked={addBreakfast}
                    id="breakfast"
                    disabled={addBreakfast}
                    onChange={() => {
                        setBreakfast((addBreakfast) => !addBreakfast);
                        setConfirmed(false);
                    }}
                >
                    Want to add breakfast for {formatCurrency(optionalBreakfastPrice)}?
                </Checkbox>
            </Box>}
            <Box>
                <Checkbox
                    checked={confirmed}
                    id={bookingId}
                    disabled={confirmed}
                    onChange={() => setConfirmed((confirmed) => !confirmed)}
                >
                    I confirm that {guests.fullName} has paid the total amount
                    of {!addBreakfast ? formatCurrency(totalPrice) : `${formatCurrency(totalPrice + optionalBreakfastPrice)} (${formatCurrency(totalPrice)} + ${formatCurrency(optionalBreakfastPrice)})`}
                </Checkbox>
            </Box>

            <ButtonGroup>
                <Button disabled={!confirmed || isChecking} onClick={handleCheckin}>Check in booking
                    #{bookingId}</Button>
                <Button variation="secondary" onClick={moveBack}>
                    Back
                </Button>
            </ButtonGroup>
        </>
    );
}

export default CheckinBooking;
