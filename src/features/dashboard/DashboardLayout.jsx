import styled from "styled-components";
import {useRecentBooking} from "./useRecentBooking.js";
import Spinner from "../../ui/Spinner.jsx";
import {useRecentStays} from "./useRecentStays.js";
import Stats from "./Stats.jsx";
import {useLoadCabin} from "../cabins/useLoadCabin.js";
import SalesChart from "./SalesChart.jsx";
import DurationChart from "./DurationChart.jsx";
import TodayActivity from "../check-in-out/TodayActivity.jsx";

const StyledDashboardLayout = styled.div`
    display: grid;
    grid-template-columns: 1fr 1fr 1fr 1fr;
    grid-template-rows: auto 34rem auto;
    gap: 2.4rem;
`;

function DashboardLayout() {
    const {isLoading, bookings} = useRecentBooking();
    const {isLoading: isLoading2, stays, confirmedStays, numDays} = useRecentStays();
    const {cabins, isLoading: isLoading3} = useLoadCabin();
    if (isLoading || isLoading2 || isLoading3) return <Spinner/>
    return (
        <StyledDashboardLayout>
            <Stats bookings={bookings} confirmedStays={confirmedStays} numDays={numDays} cabinCount={cabins.length}>Statistics</Stats>
            <TodayActivity  />
            <DurationChart  confirmedStays={confirmedStays}/>
            <SalesChart bookings={bookings} numDays={numDays} />
        </StyledDashboardLayout>
    );
}

export default DashboardLayout;