import Form from '../../ui/Form';
import FormRow from '../../ui/FormRow';
import Input from '../../ui/Input';
import {useSettings} from "./useSettings.js";
import Spinner from "../../ui/Spinner.jsx";
import {useUpdateSetting} from "./useUpdateSetting.js";

function UpdateSettingsForm() {
    const {isLoading, settings: {miBookingLength, maxBookingLength, maxGuestsPerBooking, breakfastPrice} = {}, error}  = useSettings();
   const {isUpdatingSetting, updateSetting} =  useUpdateSetting();
    function onChangeSetting(e, field){
        const {value} = e.target
        updateSetting({[field]: value});
    }
    if(isLoading) return <Spinner />
  return (
    <Form>
      <FormRow label='Minimum nights/booking'>
        <Input type='number' id='min-nights' disabled={isUpdatingSetting} defaultValue={miBookingLength} onBlur={(e)=> onChangeSetting(e,'miBookingLength')}/>
      </FormRow>
      <FormRow label='Maximum nights/booking'>
        <Input type='number' id='max-nights' disabled={isUpdatingSetting} defaultValue={maxBookingLength} onBlur={(e)=> onChangeSetting(e,'maxBookingLength')}/>
      </FormRow>
      <FormRow label='Maximum guests/booking'>
        <Input type='number' id='max-guests' disabled={isUpdatingSetting} defaultValue={maxGuestsPerBooking} onBlur={(e)=> onChangeSetting(e,'maxGuestsPerBooking')}/>
      </FormRow>
      <FormRow label='Breakfast price'>
        <Input type='number' id='breakfast-price' disabled={isUpdatingSetting} defaultValue={breakfastPrice} onBlur={(e)=> onChangeSetting(e,'breakfastPrice')}/>
      </FormRow>
    </Form>
  );
}

export default UpdateSettingsForm;
