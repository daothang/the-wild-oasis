import {useMutation, useQueryClient} from "@tanstack/react-query";
import {createEditCabin} from "../../services/apiCabins.js";
import toast from "react-hot-toast";
import {updateSetting as updateSettingApi} from "../../services/apiSettings.js";

export function useUpdateSetting(){
    const queryClient = useQueryClient();

    const {isLoading: isUpdatingSetting, mutate:updateSetting} = useMutation({
        // mutationFn: createEditCabin,
        mutationFn: (setting) => updateSettingApi(setting),
        onSuccess: data => {
            toast.success("Setting successfully updating");
            queryClient.invalidateQueries({queryKey: ['settings']});
        },
        onError: error => {
            toast.error(error.message);

        }
    })
    return {isUpdatingSetting, updateSetting};
}