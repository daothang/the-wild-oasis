import {useOutsideClick} from "./useOutsideClick.js";
import {usePressEscape} from "./usePressEscape.js";

export function useCloseModal(handler, listenCapturing = true){
   const ref = useOutsideClick(handler, listenCapturing);
    usePressEscape(handler);
    return ref;
}