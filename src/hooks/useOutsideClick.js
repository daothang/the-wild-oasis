import {useEffect, useRef} from "react";

export function useOutsideClick(handler, listenCapturing = true) {
    const ref = useRef();
    useEffect(() => {
        function clickOutside(event) {
            if (ref.current && ref.current && !ref.current.contains(event.target)) {
                handler();
            }
        }

        document.addEventListener('click', clickOutside, listenCapturing)

        return () => {
            document.removeEventListener('click', clickOutside, listenCapturing);
        };
    }, [handler, ref, listenCapturing]);

    return ref;
}