import {useEffect} from "react";

export function usePressEscape(handler){
    useEffect(() => {
        function keyEscape(event){
            if (event.key === 'Escape') {
                handler();
            }
        }
        document.addEventListener('keyup', keyEscape)

        return ()=>{
            document.removeEventListener('keyup', keyEscape);
        };
    }, [handler]);
}