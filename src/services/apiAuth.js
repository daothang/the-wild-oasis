import supabase, {supabaseUrl} from "./superbase.js";


export async function updateUser({fullName, avatar, password}) {
    let updatingData;
    if (fullName) updatingData = {data: {fullName}};
    if (password) updatingData = {password}

    const {data: user, error} = await supabase.auth.updateUser(updatingData);
    if (error) {
        console.error(error);
        throw new Error("Signup wasn't successful");
    }
    if (!avatar) return user;

    //UPLOAD AVATAR TO SUPABASE STORAGE
    const fileName = `avatar-${Math.random()}-${avatar.name}`;
    const {error: errorAvatar} = await supabase.storage.from("avatars").upload(fileName, avatar);

    if (errorAvatar) {
        console.error(errorAvatar);
        throw new Error("Avatar uploading wasn't successful");
    }

    //Update avatar user
    const {
        data: updateAvatarUser,
        error: error2
    } = await supabase.auth.updateUser({data: {avatar: `${supabaseUrl}/storage/v1/object/public/avatars/${fileName}`}})
    if (error2) {
        console.error(error2);
        throw new Error("Avatar user hasn't successful");
    }
    return updateAvatarUser;
}

export async function signup({email, password, fullName}) {
    const {data, error} = await supabase.auth.signUp({
        email, password, options:
            {
                data:
                    {
                        fullName,
                        avatar: ""
                    }
            }
    })
    if (error) {
        console.error(error);
        throw new Error("Signup wasn't successful");
    }

    return data;
}

export async function loginApi({email, password}) {
    const {data, error} = await supabase.auth.signInWithPassword({
        email,
        password
    })
    if (error) {
        console.error(error);
        throw new Error("Login wasn't successful");
    }
    return data;
}

export async function getCurrentUser() {

    const {data: {session}} = await supabase.auth.getSession();
    if (!session) {
        return null;
    }

    const {data, error} = await supabase.auth.getUser();
    if (error) {
        throw new Error(error.message);
    }
    return data?.user
}

export async function logout() {
    const {error} = await supabase.auth.signOut();
    if (error) throw new Error(error.message);
}