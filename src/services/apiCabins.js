import supabase, {supabaseUrl} from "./superbase.js";
import {PAGE_SIZE} from "../utils/constants.js";

export async function getCabins({page}) {

    let query = supabase
        .from('cabins')
        .select('*', {count: "exact"});

    if(page){
        const from = page === 1 ? (page - 1) * (PAGE_SIZE -1) : (page - 1) * PAGE_SIZE
        const to = from + PAGE_SIZE;
        console.log(from, to)
        query = query.range(from, to);
    }
    const {data: cabins, error, count} = await query;
    if (error) {
        console.error(error)
        throw new Error("Cabins could not be loaded");
    }
    return {cabins, count};
}

export async function deleteCabin(id) {
    const {error} = await supabase
        .from('cabins')
        .delete()
        .eq('id', id);

    if (error) {
        console.error(error)
        throw new Error("Cabins could not be deleted");
    }
}

export async function createEditCabin(newCabin, id) {

    const hasImagePath = typeof newCabin.image === 'string' ? newCabin.image?.startsWith(supabaseUrl) : false;

    const imageName = `${Math.random()}-${newCabin.image.name}`.replace("/", "").replaceAll(" ", "-");

    const imagePath = hasImagePath ? newCabin.image : `${supabaseUrl}/storage/v1/object/public/cabins/${imageName}`;

    // 1. Create a cabin
    let query = supabase.from('cabins');

    // A. Create
    if (!id) {
        query = query.insert([{...newCabin, image: imagePath}]);
    }

    // B. Edit
    if (id) {
        query = query.update({...newCabin, image: imagePath}).eq('id', id);
    }

    const {data, error} = await query.select().single();
    if (error) {
        console.error(error)
        throw new Error("Cabins could not be created");
    }


    // 2. Upload image
    if (hasImagePath) {
        return data;
    }
    const {error: storageError} = await supabase.storage.from('cabins').upload(imageName, newCabin.image);

    // 3. Delete the cabin if there was an error uploading image
    if (storageError) {
        await supabase.from('cabins').delete().eq("id", data.id);
        console.log(storageError);
        throw new Error('cabin image could not be upload and the cabin was not created')
    }
    return data;


}