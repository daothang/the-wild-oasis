import React, {useState} from 'react';
import {Outlet} from "react-router-dom";
import Sidebar from "./Sidebar.jsx";
import Header from "./Header.jsx";
import styled from "styled-components";

const StyledAppLayout = styled.div`
  display: grid;
  grid-template-columns: ${({ isSidebarCollapsed }) =>
          isSidebarCollapsed ? '0 1fr' : '26rem 1fr'};
  grid-template-rows: auto 1fr;
  height: 100vh;
`

const Main = styled.main`
  padding: 4rem 4.8rem 6.4rem;
  overflow: auto;
  background-color: var(--color-grey-50);
`

const Container = styled.div`
  max-width: 120rem;
  margin: 0 auto;
  display: flex;
  flex-direction: column;
  gap: 3.2rem;
`

function AppLayout() {
    const [isSidebarCollapsed, setIsSidebarCollapsed] = useState(false);

    const toggleSidebar = () => {
        setIsSidebarCollapsed(!isSidebarCollapsed);
    };
    return (
        <>
            <StyledAppLayout isSidebarCollapsed={isSidebarCollapsed}>
                <Header toggleSidebar={toggleSidebar} isSidebarCollapsed={isSidebarCollapsed}/>
                <Sidebar/>
                <Main>
                    <Container>
                        <Outlet/>
                    </Container>
                </Main>
            </StyledAppLayout></>
    );
}

export default AppLayout;