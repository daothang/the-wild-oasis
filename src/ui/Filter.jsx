import styled, {css} from "styled-components";
import {useSearchParams} from "react-router-dom";

const StyledFilter = styled.div`
    border: 1px solid var(--color-grey-100);
    background-color: var(--color-grey-0);
    box-shadow: var(--shadow-sm);
    border-radius: var(--border-radius-sm);
    padding: 0.4rem;
    display: flex;
    gap: 0.4rem;
`;

const FilterButton = styled.button`
    background-color: var(--color-grey-0);
    border: none;

    ${(props) =>
            props.active &&
            css`
                background-color: var(--color-brand-600);
                color: var(--color-brand-50);
            `}

    border-radius: var(--border-radius-sm);
    font-weight: 500;
    font-size: 1.4rem;
    /* To give the same height as select */
    padding: 0.44rem 0.8rem;
    transition: all 0.3s;

    &:hover:not(:disabled) {
        background-color: var(--color-brand-600);
        color: var(--color-brand-50);
    }
`;

function Filter({field, options}) {
    const [searchParams, setSearchParams] = useSearchParams();

    const currentFilter = searchParams.get(field) || options.at(0).value

    function handleFilter(value) {
        searchParams.set(field, value);

        //Reset page to 1
        if (searchParams.get("page")) {
            setSearchParams(searchParams);
        }
        setSearchParams(searchParams);
    }

    return (
        <StyledFilter>
            {options.map(option => <FilterButton
                active={currentFilter === option.value ? 1 : 0}
                disabled={currentFilter === option.value}
                onClick={() => handleFilter(option.value)}
                key={option.value}>
                {option.label}
            </FilterButton>)}
        </StyledFilter>
    );
}

export default Filter;