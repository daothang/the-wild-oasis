import React from 'react';
import styled from "styled-components";
import HeaderMenu from "./HeaderMenu.jsx";
import UserAvatar from "./UserAvatar.jsx";
import ButtonIcon from "./ButtonIcon.jsx";
import Button from "./Button.jsx";
import {MdOutlineMenu, MdOutlineMenuOpen} from "react-icons/md";
import {RiMenuUnfoldLine} from "react-icons/ri";

const StyledHeader = styled.header`
  background-color: var(--color-grey-0);
  padding: 1.2rem 4.8rem;
  border-bottom: 1px solid var(--color-grey-100);
  display: flex;
  justify-content: space-between;
  align-items: center;
`
const StyledRow = styled.div`
  background-color: var(--color-grey-0);
  padding: 1.2rem 4.8rem;
  border-bottom: 1px solid var(--color-grey-100);
  display: flex;
  gap: 2rem;
`

function Header({toggleSidebar, isSidebarCollapsed}) {
    return (<StyledHeader>
            <Button onClick={toggleSidebar}>
                {isSidebarCollapsed ? <RiMenuUnfoldLine/> : <MdOutlineMenuOpen/> }
            </Button>
            <StyledRow>
                <UserAvatar/>
                <HeaderMenu/>
            </StyledRow>
        </StyledHeader>);
}

export default Header;