import React from 'react';
import styled from "styled-components";


const Input = styled.input`
border: 1px solid var(--color-grey-300);
background-color: var(--color-grey-0);
border-radius: 0.5rem 0.5rem;
box-shadow: var(--shadow-sm)`

export default Input;