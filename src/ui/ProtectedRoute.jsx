import React, {useEffect} from 'react';
import {useUser} from "../features/authentication/useUser.js";
import styled from "styled-components";
import Spinner from "./Spinner.jsx";
import {useNavigate} from "react-router-dom";

const FullPage = styled.div`
    height: 100dvh;
    background-color: var(--color-grey-50);
    display: flex;
    align-content: center;
    align-items: center;
`

function ProtectedRoute({children}) {
    const navigate = useNavigate();
    //1. Load the authenticated user
    const {isLoading, isAuthenticated} = useUser();

    //2. If there is NO authenticated user, redirect to the login
    useEffect(function () {
        if (!isAuthenticated && !isLoading) {
            navigate("/login");
        }
    }, [isLoading, isAuthenticated, navigate])


    //3. While loading, show a spinner
    if (isLoading) return <FullPage>
        <Spinner/>
    </FullPage>
    //4. If there Is a user, render APP
    if (isAuthenticated)
        return children
}

export default ProtectedRoute;